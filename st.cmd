# ---
require essioc
require bursterprecision1427

epicsEnvSet(IPADDR, "172.30.32.11")
epicsEnvSet(IPPORT, "4001")
epicsEnvSet(LOCATION, "SE: $(IPADDR):$(IPPORT)")
epicsEnvSet(PREFIX, "SE-SEE:SE-BU1427-001")
epicsEnvSet(SCAN, "5")
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(bursterprecision1427_DIR)db")

iocshLoad("$(essioc_DIR)/common_config.iocsh")
iocshLoad("$(bursterprecision1427_DIR)bursterprecision1427.iocsh", "PREFIX=$(PREFIX), IPADDR=$(IPADDR), IPPORT=$(IPPORT), SCAN=$(SCAN)")
# ...
